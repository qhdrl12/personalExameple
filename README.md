# Installation Guide

Personal requires Springboot(mvn)

```sh
$ cd  personalExample
$ mvn spring-boot:run
```
> springfox swagger-core, swagger-ui for RestApi

> swagger-core :  http://domain/v2/api-docs

> swagger-ui  :  http://domain/swagger-ui.html

### docker-compose.yml 수정
MYSQL
- ports 
- environment
    * MYSQL_ROOT_PASSWORD
    * MYSQL_DATABASE
    * MYSQL_USER
    * MYSQL_PASSWORD

APP
- ports
- environment
    * DATABASE_HOST
    * DATABASE_USER
    * DATABASE_PASSWORD
    * DATABASE_NAME
    * DATABASE_PORT

mysqlSchemaSql.md 스크립트 실행


### Docker-Compose Install
```sh
$ sudo curl -L https://github.com/docker/compose/releases/download/1.13.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
    
$ docker-compose --version

```

### /etc/hosts 수정 

```sh 
CONTAINER_IP
$ ps -ef | grep container-ip

CONTAINER_ID
$ sudo docker ps 
```

CONTAINER_IP mysql CONTAINER_ID basic-mysql 


### Run 
- Notice - 3307(외부), 3306(내부)

```sh
$ sudo docker-compose up
```

### 참고

App Image
- https://hub.docker.com/r/qhdrl12/springboot/
