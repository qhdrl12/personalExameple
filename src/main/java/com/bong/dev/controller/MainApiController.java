package com.bong.dev.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.bong.dev.domain.User;
import com.bong.dev.handler.exception.MainException;
import com.bong.dev.service.UserService;

@RestController
@RequestMapping("/api/user")
public class MainApiController {

	private static final Logger logger = LoggerFactory.getLogger(MainApiController.class);

	private static final String MESSAGE_NO_CONTENT = "조회 결과가 없습니다.";
	private static final String MESSAGE_NOT_FOUND = "사용자를 찾을 수 없습니다.";
	private static final String MESSAGE_CONFLICT = "중복 데이터가 존재합니다.";

	@Autowired
	UserService mainService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<List<User>> allUsers() throws MainException {

		List<User> users = mainService.findAll();

		if (users.isEmpty()) {
			throw new MainException(MESSAGE_NO_CONTENT, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> getUser(@PathVariable("id") int id) throws MainException {
		logger.info("query string : " + id);

		User user = mainService.findById(id);
		if (user == null) {
			throw new MainException(MESSAGE_NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<Void> createUser(@RequestBody User user, UriComponentsBuilder ucBuilder) throws MainException {
		logger.info("query string : " + user);

		if (mainService.isUserExist(user)) {
			throw new MainException(MESSAGE_CONFLICT, HttpStatus.CONFLICT);
		}

		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		mainService.save(user);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public ResponseEntity<User> updateUser(@PathVariable("id") int id, @RequestBody User user) throws MainException {
		logger.info("query string : " + id + " : " + user);

		User currentUser = mainService.findById(id);

		if (currentUser == null) {
			throw new MainException(MESSAGE_NOT_FOUND, HttpStatus.NOT_FOUND);
		}

		currentUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

		mainService.update(currentUser);
		return new ResponseEntity<User>(currentUser, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<User> deleteUser(@PathVariable("id") int id) throws MainException {
		logger.info("query string : " + id);

		User user = mainService.findById(id);

		if (user == null) {
			throw new MainException(MESSAGE_NOT_FOUND, HttpStatus.NOT_FOUND);
		}

		mainService.delete(id);
		return new ResponseEntity<User>(user, HttpStatus.NO_CONTENT);
	}
}
