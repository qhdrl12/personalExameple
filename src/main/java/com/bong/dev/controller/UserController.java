package com.bong.dev.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bong.dev.domain.User;
import com.bong.dev.service.UserServiceImpl;

@Controller
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserServiceImpl userService;

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String registration(Model model) {
		model.addAttribute("userForm", new User());

		return "auth/registration";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String registreation(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
		logger.info("reuqest parameter : " + userForm);
		userService.save(userForm);

		return "redirect:/auth/login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, String error, String logout) {

		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");

		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");

		return "auth/login";
	}

	@RequestMapping(value = {
		"/", "/welcome"
	}, method = RequestMethod.GET)
	public String welcome() {

		return "welcome";
	}

}
