package com.bong.dev.dao;

import java.util.List;

import com.bong.dev.domain.User;

public interface UserMapper {

	public List<User> findAll();

	public User findById(long id);

	public User findByUserId(String userid);

	public User findByUsername(String username);

	public int save(User user);

	public void update(User user);

	public void delete(long id);
}
