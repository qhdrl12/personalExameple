package com.bong.dev.common;

public enum ResultCode {
	SUCCESS(0, "정상");

	private final int code;
	private final String description;

	ResultCode(int code, String description) {
		this.code = code;
		this.description = description;
	}

	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return code + ": " + description;
	}

	public static ResultCode valueOf(int code) {
		for (ResultCode rc : values()) {
			if (rc.code == code) {
				return rc;
			}
		}
		throw new IllegalArgumentException("No matching constant for [" + code + "]");
	}
}
