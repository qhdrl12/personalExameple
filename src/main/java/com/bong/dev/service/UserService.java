package com.bong.dev.service;

import java.util.List;

import com.bong.dev.domain.User;

public interface UserService {
	List<User> findAll();

	User findById(long id);

	User findByUserId(String id);

	User findByUsername(String username);

	long save(User user);

	void update(User user);

	void delete(long id);

	boolean isUserExist(User user);
}
