package com.bong.dev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bong.dev.dao.UserMapper;
import com.bong.dev.domain.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public List<User> findAll() {
		return userMapper.findAll();
	}

	@Override
	public User findById(long id) {
		return userMapper.findById(id);
	}

	@Override
	public User findByUserId(String userid) {
		return userMapper.findByUserId(userid);
	}

	@Override
	public User findByUsername(String username) {
		return userMapper.findByUsername(username);
	}

	@Override
	public long save(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		return userMapper.save(user);
	}

	@Override
	public void update(User user) {
		userMapper.update(user);
	}

	@Override
	public void delete(long id) {
		userMapper.delete(id);
	}

	@Override
	public boolean isUserExist(User user) {
		return findById(user.getId()) != null;
	}
}
