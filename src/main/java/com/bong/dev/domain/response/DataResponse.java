package com.bong.dev.domain.response;

public class DataResponse<T> extends Response {
	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
