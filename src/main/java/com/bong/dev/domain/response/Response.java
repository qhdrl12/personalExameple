package com.bong.dev.domain.response;

import com.bong.dev.domain.BaseObject;

public class Response extends BaseObject {

	private int result;
	private String message;

	public Response(int result, String message) {
		this.result = result;
		this.message = message;
	}

	public Response() {
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
