package com.bong.dev.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author qhdrl12
 *
 */
@ApiModel(value = "user", description = "TODO")
public class User extends BaseObject {

	@ApiModelProperty(hidden = true)
	private long id;
	@ApiModelProperty(required = false, example = "qhdrl12")
	private String userid;
	@ApiModelProperty(required = false, example = "BongKi")
	private String username;
	@ApiModelProperty(required = false, example = "1234qwer")
	private String password;
	@ApiModelProperty(required = false, example = "qhdrl@gmail.com")
	private String email;
	@ApiModelProperty(required = false, example = "1")
	private String enable;
	@ApiModelProperty(hidden = true)
	private Role role = Role.USER;
	@ApiModelProperty(hidden = true)
	private String createDate;
	@ApiModelProperty(hidden = true)
	private String modifyDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}

}
