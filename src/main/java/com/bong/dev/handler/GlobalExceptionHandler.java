package com.bong.dev.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.bong.dev.domain.response.Response;
import com.bong.dev.handler.exception.MainException;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	public @ResponseBody ResponseEntity<Response> exceptionMainHandler(MainException ex) {
		Response error = new Response(ex.getErrorStatus().value(), ex.getMessage());
		return new ResponseEntity<Response>(error, ex.getErrorStatus());
	}

	@ExceptionHandler(value = {
		HttpMessageNotReadableException.class, MethodArgumentTypeMismatchException.class
	})
	public @ResponseBody ResponseEntity<Response> HttpMessageNotReadableExceptionHandler(Exception ex) {
		Response error = new Response(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
		return new ResponseEntity<Response>(error, HttpStatus.BAD_REQUEST);
	}

}
