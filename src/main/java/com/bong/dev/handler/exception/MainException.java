package com.bong.dev.handler.exception;

import org.springframework.http.HttpStatus;

public class MainException extends Exception {

	private static final long serialVersionUID = -8944924777072008990L;

	private HttpStatus errorStatus;
	private String errorMessage;

	public MainException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public MainException(String errorMessage, HttpStatus errorStatus) {
		super(errorMessage);
		this.errorMessage = errorMessage;
		this.errorStatus = errorStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public HttpStatus getErrorStatus() {
		return errorStatus;
	}

	public MainException() {
		super();
	}

}
