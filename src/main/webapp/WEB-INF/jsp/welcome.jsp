<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> 

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create an account</title>

    <link rel="stylesheet" href="<s:url value="/webjars/bootstrap/3.3.4/dist/css/bootstrap.min.css"/>">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">

	
	<sec:authorize access="isAuthenticated()">
<%--     <c:if test="${pageContext.request.userPrincipal.name != null}"> --%>
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome ${pageContext.request.userPrincipal.name} <!--<sec:authentication property="principal.authorities"/>--> | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>
	</sec:authorize>
<%--     </c:if> --%>

</div>
<!-- /container -->
<script src="<s:url value="/webjars/jQuery/3.1.1/dist/jquery.min.js"/>"></script>
<script src="<s:url value="/webjars/bootstrap/3.3.4/dist/js/bootstrap.min.js"/>"></script>
</body>
</html>