// package com.bong.dev;
//
// import static org.hamcrest.CoreMatchers.is;
// import static org.junit.Assert.assertThat;
// import static org.junit.Assert.assertTrue;
//
// import org.junit.Before;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.context.ApplicationContext;
// import org.springframework.test.context.junit4.SpringRunner;
// import org.springframework.transaction.annotation.Transactional;
//
// import com.bong.dev.dao.MainMapper;
// import com.bong.dev.domain.User;
//
// @RunWith(SpringRunner.class)
// @SpringBootTest
// @Transactional
// public class MainDaoTest {
//
// @Autowired
// ApplicationContext context;
//
// @Autowired
// MainMapper mainMapper;
//
// static User user;
//
// @Before
// public void setup() {
// user = new User("1", "bong", "bong", "1234qwer");
// }
//
// @Test
// public void userAddAndGet() {
//
// mainMapper.saveUser(user);
//
// User user2 = mainMapper.findById(user.getId());
//
// assertThat(user2.getName(), is(user.getName()));
// assertThat(user2.getPassword(), is(user.getPassword()));
// }
//
// @Test
// public void test() {
// assertTrue(context.getBean("mainMapper") == mainMapper);
// }
// }
