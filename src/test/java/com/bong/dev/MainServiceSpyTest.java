// package com.bong.dev;
//
// import static org.hamcrest.CoreMatchers.equalTo;
// import static org.hamcrest.CoreMatchers.is;
// import static org.junit.Assert.assertThat;
// import static org.mockito.Mockito.doReturn;
// import static org.mockito.Mockito.never;
// import static org.mockito.Mockito.verify;
//
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.mockito.Mock;
// import org.mockito.Spy;
// import org.mockito.runners.MockitoJUnitRunner;
//
// import com.bong.dev.dao.MainMapper;
// import com.bong.dev.domain.User;
// import com.bong.dev.service.MainServiceImpl;
//
// @RunWith(MockitoJUnitRunner.class)
// public class MainServiceSpyTest {
//
// @Spy
// private MainServiceImpl mainServiceSpy;
//
// @Mock
// private MainMapper mainMapper;
//
// @Mock
// private User user;
//
// @Test(expected = NullPointerException.class)
// public void shouldThrowNullPointerException_whenGetUserByIdIsCalled() throws Exception {
//
// User retrievedUser = mainServiceSpy.findById(5);
//
// assertThat(retrievedUser, is(equalTo(user)));
// }
//
// @Test
// public void shouldThrowNullPointerException_whenSaveUserIsCalled() throws Exception {
//
// doReturn(1).when(mainServiceSpy).saveUser(user);
//
// int savedUser = mainServiceSpy.saveUser(user);
//
// // verify(mainServiceSpy).saveUser(user);
// assertThat(savedUser, is(equalTo(1)));
// }
//
// @Test
// public void shouldVerifyThatGetUserByIdIsCalled() throws Exception {
//
// doReturn(user).when(mainServiceSpy).findById(5);
//
// User retrievedUser = mainServiceSpy.findById(5);
//
// // verify(mainServiceSpy).findById(5);
// assertThat(retrievedUser, is(equalTo(user)));
//
// }
//
// @Test
// public void shouldVerifyThatGetUserByIsNotCalled() throws Exception {
// doReturn(user).when(mainServiceSpy).findById(5);
//
// User retrievedUser = mainServiceSpy.findById(5);
//
// verify(mainServiceSpy, never()).saveUser(user);
// }
// }
