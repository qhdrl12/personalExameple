// package com.bong.dev;
//
// import static org.hamcrest.CoreMatchers.is;
// import static org.mockito.Mockito.when;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
// import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
// import java.nio.charset.Charset;
//
// import org.junit.Before;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.mockito.InjectMocks;
// import org.mockito.Mock;
// import org.mockito.runners.MockitoJUnitRunner;
// import org.springframework.http.MediaType;
// import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
// import org.springframework.test.web.servlet.MockMvc;
// import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
// import com.bong.dev.controller.MainApiController;
// import com.bong.dev.domain.User;
// import com.bong.dev.service.MainServiceImpl;
// import com.fasterxml.jackson.databind.ObjectMapper;
//
// @RunWith(MockitoJUnitRunner.class)
// public class MainControllerSpyTest {
// public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
//
// private MockMvc mockMvc;
//
// private User user;
//
// @Mock
// private MainServiceImpl mainService;
//
// @InjectMocks
// private MainApiController mainController = new MainApiController();
//
// @Before
// public void setup() throws Exception {
// this.mockMvc = MockMvcBuilders.standaloneSetup(mainController)
// .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
// }
//
// @Test
// public void getUserByIdIsCalledWithController() throws Exception {
// int id = 5;
//
// user = new User();
// user.setId(id);
// user.setName("bong");
//
// when(mainService.findById(id)).thenReturn(user);
//
// mockMvc.perform(
// get("/api/user/" + id))
// .andExpect(status().isOk()).andDo(print())
// .andExpect(content().contentType(APPLICATION_JSON_UTF8))
// .andExpect(jsonPath("name", is("bong")))
// .andExpect(jsonPath("id", is(5)));
//
// // verify(mainService).findById(5);
// }
//
// @Test
// public void saveUserIsCalledWithController() throws Exception {
// user = new User();
// user.setName("bong");
//
// String userJson = new ObjectMapper().writeValueAsString(user);
//
// mockMvc.perform(
// post("/api/user/").contentType(MediaType.APPLICATION_JSON).content(userJson)).andDo(print())
// .andExpect(status().isCreated())
// .andExpect(content().contentType(APPLICATION_JSON_UTF8));
// }
//
// @Test
// public void deleteUserIsCalledWithController() throws Exception {
// user = new User();
// user.setName("bong");
//
// int id = 1;
//
// when(mainService.findById(id)).thenReturn(user);
//
// mockMvc.perform(
// delete("/api/user/1").contentType(MediaType.APPLICATION_JSON)).andDo(print())
// .andExpect(status().isNoContent())
// .andExpect(content().contentType(APPLICATION_JSON_UTF8));
// }
// }
